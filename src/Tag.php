<?php 

namespace Fifteen\MetaTags;

use Fifteen\HtmlElement\HtmlElement;

class Tag
{
    protected $tagName = 'meta';
    protected $element;

    public function __construct($key, $value_or_attributes, $tagName = null, $keyName = null, $valueName = null)
    {
        $this->setTagName($tagName);
        $this->setKeysAndValues($key, $value_or_attributes, $keyName, $valueName);
    }

    private function setKeysAndValues($key, $value_or_attributes, $keyName, $valueName)
    {
        if (is_array($value_or_attributes)) {
            $attributes = $value_or_attributes;
        } else {
            $attributes = [
                $keyName ?: 'name' => $key,
                $valueName ?: 'content' => $value_or_attributes
            ];
        }
        switch ($this->getTagName()) {
            case 'title':
                $value = $value_or_attributes;
                $this->element = HtmlElement::create($this->getTagName(), $value);
                break;
            default: 
                $this->element = HtmlElement::createVoid($this->getTagName(), $attributes);
        }
    }

    public function render()
    {
        return $this->element->render();
    }

    /**
    *   Setter methods:
    **/

    public function setElement($item)
    {
        $this->element = $item;
    }

    public function setTagName($item = null)
    {
        $this->tagName = empty($item) ? 'meta' : $item;
    }


    /**
    *   Getter methods:
    **/

    public function getElement()
    {
        return $this->element;
    }

    public function getTagName()
    {
        return $this->tagName;
    }

}