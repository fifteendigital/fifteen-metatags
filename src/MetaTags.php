<?php 

namespace Fifteen\MetaTags;

use Fifteen\MetaTags\Tag;

class MetaTags
{
    protected $tags = [];

    public function __construct()
    {
    }

    public function get($name = null)
    {
        if ($name == null) {                    // return all tags
            return $this->tags;
        } else {                                // return tag with key provided
            if (isset($this->tags[$name])) {
                return $this->tags[$name];
            }
        }
    }

    /**
    * $value_or_attributes = the value for the 'content' [or user defined using valueName] attribute, or an array of keys => values for attributes
    **/
    public function register($key, $value_or_attributes, $tagName = null, $keyName = null, $valueName = null)
    {
        $this->tags[$key] = new Tag($key, $value_or_attributes, $tagName, $keyName, $valueName);
    }

    public function registerTitle($value)
    {
        $this->register('title', $value, 'title');
    }
    
    public function registerDescription($value)
    {
        $this->register('description', $value);
    }

    public function registerKeywords($value)
    {
        $this->register('keywords', $value);
    }

    public function registerOgTag($key, $value)
    {
        if (stripos($key, 'og:') === false) {
            $key = 'og:' . $key;
        }
        $this->register($key, $value, null, 'property');
    }

    public function render()
    {
        $output = '';
        foreach ($this->tags as $key => $tag) {
            $output .= "\n" . $tag->render();
        }
        return $output;
    }

    public function __get($name) 
    {
        return $this->get($name);
    }

    public function __call($function, $parameters) 
    {
        return $this->get($function);
    }
}