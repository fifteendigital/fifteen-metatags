# Metatags

A really simple package that helps manage and render HTML meta tags. 
It uses the fifteen/htmlElement package to create and render the tags.

###Installation

Add Metatags to your composer.json file:

    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.1.*",
        "fifteen/metatags": "1.0.*"
    },

Add minimum-stability: dev:
```sh
    "minimum-stability": "dev",
```

As the repository isn't on Packagist, you also need to include it in the repositories list, along with fifteen/htmlelement:

```php
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-htmlelement.git"
        },
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-metatags.git"
        }
    ]
```

Run composer update:

```sh
> composer update
```

Register service provider in config/app.php:

```php
    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        ...

        /*
         * Package Service Providers...
         */
        Fifteen\MetaTags\MetaTagsServiceProvider::class,
    ];
```

Add MetaTags to the aliases:
```php
    'MetaTags'  => Fifteen\MetaTags\Facades\MetaTags::class,
```

### Usage
You can create meta tags using the _register_ method:
```php
public function register($key, $value_or_attributes, $tagName = null, $keyName = null, $valueName = null)
```
Example:
```php
MetaTags::register('author', 'James Tierney');

echo Metatags::render();
```
Outputs
```html
<meta name="author" content="James Tierney" />
```

### Helper methods
There are in addition several helper methods which can be used for common meta tags:
```php
MetaTags::registerTitle($title);
MetaTags::registerDescription($description);
MetaTags::registerKeywords($keywords);
MetaTags::registerOgTag($key, $value);
```